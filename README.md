# "Theta oscillations and minor hallucinations in Parkinson’s disease reveal decrease in frontal lobe functions and later cognitive decline"

Here you can find code and data of our study "Theta oscillations and minor hallucinations in Parkinson’s disease reveal decrease in frontal lobe functions and later cognitive decline" 

# General info:
- Authors: Fosco Bernasconi, Javier Pagonabarraga, Helena Bejr-Kasem, Saul Martinez-Horta, Juan Marín-Lahoz, Andrea Horta-Barba, Jaime Kulisevsky, Olaf Blanke
- Journal: Nature Mental Health
- DOI: 10.1038/s44220-023-00080-6
- Contact: fosco.bernasconi@gmail.com 

# Codes:

- ./Code/rstudio/EEG/analyses/: code to analyse periodic and aperiodic signals
- ./Code/rstudio/clinical_demographic/: code for clinical and demographic analyses


# Data:

- ./Data/EEG/: Periodic and aperiodic signals
- ./Data/Clinical/: Clinical and demographic data