---
title: "Spectral parameterization for studying minor hallucinations in PD"
author: "Fosco Bernasconi"
date: "6/22/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
 knitr::knit_engines$set(python = reticulate::eng_python)
```

### Load R libraries
```{r, message= FALSE}
library(tidyverse)   # Access a collection of packages for data management (e.g., dplyr, ggplot2, readr)
library(reticulate)  # Interface Python and R Studio
library(gridExtra)   # Arrange multiple plots
library(psych)       # Toolbox for data analysis
library(sjstats)     # Toolbox for data analysis
library(ggpubr)      # Additional tools for data visualization
library(cowplot)
library(here)
library(broom)
library(MASS)
library(sfsmisc)
library(lmPerm)
library(permuco)
library(perm)
library(kableExtra)
library(tidyr)
library(dplyr)
library(purrr)
library(lme4)
library(lmerTest)
library(sjPlot)
library(predictmeans)
library(eegUtils)
library(data.table)
library(here)
```

### Load data
```{r, message= FALSE}
dataname = here("Data","EEG","Fooof_data_PD.csv")
dat = fread(dataname)
```


### Power: Interaction sub-group - UPDRS-3
```{r, warning= FALSE}
## theta
set.seed(4375)
tpower = list()
tpower = dat %>% 
  dplyr::select(PW_theta,MH,updrs3,Chan) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::lmperm(PW_theta ~ MH*updrs3, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled Pr(>|t|)')) %>%
  mutate(pparam = map(perf,'parametric Pr(>|t|)')) %>%
  mutate(F= map(perf,'t value')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(tpower)
Fs=as_tibble(matrix(unlist(allres$F),ncol=4,byrow = T)); names(Fs) = c('NA','MH','updrs3','interaction')
ps=as_tibble(matrix(unlist(allres$pperm),ncol=4,byrow = T)); names(ps) = c('NA','pMH','pupdrs3','pinteraction')
widestats = allres %>% dplyr::select(-F,-pperm) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(interaction)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pinteraction))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))  %>% ungroup()
longstats = longstats %>% mutate_if(is.numeric, round,2)
write.table(longstats, file = "~/Theta_interaction_updrs3.txt", sep = ",", quote = FALSE, row.names = F)
rm(longstats)

## Alpha
set.seed(999)
apower = list()
apower = dat %>% 
  dplyr::select(PW_alpha,MH,updrs3,Chan) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::lmperm(PW_alpha ~ MH*updrs3, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled Pr(>|t|)')) %>%
  mutate(pparam = map(perf,'parametric Pr(>|t|)')) %>%
  mutate(F= map(perf,'t value')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(apower)
Fs=as_tibble(matrix(unlist(allres$F),ncol=4,byrow = T)); names(Fs) = c('NA','MH','updrs3','interaction')
ps=as_tibble(matrix(unlist(allres$pperm),ncol=4,byrow = T)); names(ps) = c('NA','pMH','pupdrs3','pinteraction')
widestats = allres %>% dplyr::select(-F,-pperm) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(interaction)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pinteraction))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))  %>% ungroup()
longstats = longstats %>% mutate_if(is.numeric, round,2)
write.table(longstats, file = "~/Alpha_interaction_updrs3.txt", sep = ",", quote = FALSE, row.names = F)
rm(longstats)

## Beta
set.seed(1165)
bpower = list()
bpower = dat %>% 
  dplyr::select(PW_beta,MH,updrs3,Chan) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::lmperm(PW_beta ~ MH*updrs3, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled Pr(>|t|)')) %>%
  mutate(pparam = map(perf,'parametric Pr(>|t|)')) %>%
  mutate(F= map(perf,'t value')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(bpower)
Fs=as_tibble(matrix(unlist(allres$F),ncol=4,byrow = T)); names(Fs) = c('NA','MH','updrs3','interaction')
ps=as_tibble(matrix(unlist(allres$pperm),ncol=4,byrow = T)); names(ps) = c('NA','pMH','pupdrs3','pinteraction')
widestats = allres %>% dplyr::select(-F,-pperm) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(interaction)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pinteraction))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))  %>% ungroup()
longstats = longstats %>% mutate_if(is.numeric, round,2)
write.table(longstats, file = "~/Beta_interaction_updrs3.txt", sep = ",", quote = FALSE, row.names = F)
rm(longstats)

## Gamma
set.seed(9865)
gpower = list()
gpower = dat %>% 
  dplyr::select(PW_gamma,MH,updrs3,Chan) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::lmperm(PW_gamma ~ MH*updrs3, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled Pr(>|t|)')) %>%
  mutate(pparam = map(perf,'parametric Pr(>|t|)')) %>%
  mutate(F= map(perf,'t value')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(gpower)
Fs=as_tibble(matrix(unlist(allres$F),ncol=4,byrow = T)); names(Fs) = c('NA','MH','updrs3','interaction')
ps=as_tibble(matrix(unlist(allres$pperm),ncol=4,byrow = T)); names(ps) = c('NA','pMH','pupdrs3','pinteraction')
widestats = allres %>% dplyr::select(-F,-pperm) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(interaction)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pinteraction))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))  %>% ungroup()
longstats = longstats %>% mutate_if(is.numeric, round,2)
write.table(longstats, file = "~/Gamma_interaction_updrs3.txt", sep = ",", quote = FALSE, row.names = F)
rm(longstats)
```

